import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

//import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords

import internal.GlobalVariable as GlobalVariable


public class func {

	static String url = GlobalVariable.url_to
	static String randomString(int length) {

		String chars = 'abcdefghijklmnopqrstuvwxyz0123456789'

		Random rand = new Random()

		StringBuilder sb = new StringBuilder()

		for (int i = 0; i < length; i++) {
			sb.append(chars.charAt(rand.nextInt(chars.length())))
		}

		return sb.toString()
	}

	static void signin(String login) {

		WebUI.openBrowser(url)

		//WebUI.navigateToUrl(url)

		WebUI.setText(findTestObject('Page_Login/input_username'), login)

		WebUI.setText(findTestObject('Page_Login/input_password'), '12345')

		WebUI.click(findTestObject('Page_Login/button_login'))
	}
}
