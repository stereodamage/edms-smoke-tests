import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory
import func

String assignmentDescription = func.randomString(10)
String url = GlobalVariable.url_to.toString()
WebUI.callTestCase(findTestCase('Basic Cases/Registrator Login Case (Korchagin)'), [:], FailureHandling.STOP_ON_FAILURE)
WebUI.click(findTestObject('Page_With_Menu/a_MyAssignments'))
WebUI.click(findTestObject('Page_Assignments_List_View/add_new_assignment_button'))
WebUI.setText(findTestObject('Page_Create_Assignments/textarea_description'), assignmentDescription)
WebUI.setText(findTestObject('Page_Create_Assignments/input_id_coimplementer_set-0-c'), 'мел')
WebUI.click(findTestObject('Page_Create_Assignments/strong_Mel'))
WebUI.click(findTestObject('Page_Create_Assignments/input__send'))
WebUI.callTestCase(findTestCase('Basic Cases/Registrator Login Case (Melnikova)'), [:], FailureHandling.STOP_ON_FAILURE)
driver = DriverFactory.getWebDriver()
WebUI.navigateToUrl(url + 'assignments2me/?is_filtering=true&description=' + assignmentDescription)
assignment = driver.findElement(By.xpath('//*[@id="table"]/tbody/tr/td//a[text()="'.concat(assignmentDescription).concat('"]')))
assignment.click()
WebUI.click(findTestObject('click_change'))
WebUI.click(findTestObject('click_answer'))
WebUI.setText(findTestObject('textarea_answer'), 'ответ')
WebUI.click(findTestObject('input__execute'))
WebUI.callTestCase(findTestCase('Basic Cases/Logout Case'), [:], FailureHandling.STOP_ON_FAILURE)
