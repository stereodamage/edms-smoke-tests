import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.Keys as Keys
import func as func

String documentContent = func.randomString(10)

String url = GlobalVariable.url_to.toString()

String filtering_url = url + 'documents/inputdocuments/?is_filtering=true&content=' + documentContent

String document_xpath = '//*[@id="table"]//td//a[text()="' + documentContent + '"]'

String change_xpath = "//a[(text() = 'Изменить' or . = 'Изменить')]"

String delete_xpath = "//a[text() = 'Удалить']"

WebUI.callTestCase(findTestCase('Basic Cases/Registrator Login Case (Registrator1)'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('document test/i_add_document'))

WebUI.click(findTestObject('document test/id_recipient_div'))

WebUI.setText(findTestObject('document test/input_id_sender-input'), 'Авиа-Л')

WebUI.click(findTestObject('document test/input_id_recipient_strong_Avia-L'))

WebUI.click(findTestObject('document test/textarea_content_label'))

WebUI.setText(findTestObject('document test/textarea_content'), documentContent)

WebUI.click(findTestObject('document test/input_id_addressee-input'))

WebUI.setText(findTestObject('document test/input_id_addressee-input'), 'Получ')

WebUI.sendKeys(findTestObject('document test/input_id_addressee-input'), Keys.chord(Keys.DOWN))

WebUI.sendKeys(findTestObject('document test/input_id_addressee-input'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('document test/input_id_responsible-input'))

WebUI.setText(findTestObject('document test/input_id_responsible-input'), 'Отв')

WebUI.sendKeys(findTestObject('document test/input_id_responsible-input'), Keys.chord(Keys.DOWN))

WebUI.sendKeys(findTestObject('document test/input_id_responsible-input'), Keys.chord(Keys.ENTER))

WebUI.click(findTestObject('document test/button_from_document_form_register_input'))

// Проверка на невозможность удаления

WebUI.navigateToUrl(filtering_url)

driver = DriverFactory.getWebDriver()

document = driver.findElement(By.xpath(document_xpath))

document.click() //detail

buttons = driver.findElements(By.xpath(delete_xpath))

if(buttons.size() != 0){
	throw new RuntimeException("Зарегистрированный документ имеет кнопку Удалить")
}

WebUI.callTestCase(findTestCase('Basic Cases/Logout Case'), [:], FailureHandling.STOP_ON_FAILURE)

func.signin('poluchatel_1')

driver = DriverFactory.getWebDriver()

WebUI.navigateToUrl(filtering_url)

document = driver.findElement(By.xpath(document_xpath))

document.click() //detail

if (driver.findElements(By.xpath(change_xpath)).size() != 0){
	throw new RuntimeException("Документ доступен к изменению Получателем 1")
}

WebUI.callTestCase(findTestCase('Basic Cases/Logout Case'), [:], FailureHandling.STOP_ON_FAILURE)

func.signin('otvetstvennyj_1')

driver = DriverFactory.getWebDriver()

WebUI.navigateToUrl(filtering_url)

document = driver.findElement(By.xpath(document_xpath))

document.click() //detail

if (driver.findElements(By.xpath(change_xpath)).size() != 0){
	throw new RuntimeException("Документ доступен к изменению ответственным 1")
}

WebUI.click(findTestObject('document test/click_create_assignment')) //create assignment

String assignmentDescription = "Поручение по ".concat(documentContent)

WebUI.setText(findTestObject('Page_Create_Assignments/textarea_description'), assignmentDescription)
//WebUI.click(findTestObject('Page_Create_Assignments/add_new_implementer_button'))
WebUI.setText(findTestObject('Page_Create_Assignments/input_id_coimplementer_set-0-c'), 'Испо')
WebUI.sendKeys(findTestObject('Page_Create_Assignments/input_id_coimplementer_set-0-c'), Keys.chord(Keys.DOWN))
WebUI.sendKeys(findTestObject('Page_Create_Assignments/input_id_coimplementer_set-0-c'), Keys.chord(Keys.ENTER))
WebUI.click(findTestObject('Page_Create_Assignments/label_Controller'))
WebUI.setText(findTestObject('Page_Create_Assignments/input_id_controller-input'), 'Контр')
WebUI.sendKeys(findTestObject('Page_Create_Assignments/input_id_controller-input'), Keys.chord(Keys.DOWN))
WebUI.sendKeys(findTestObject('Page_Create_Assignments/input_id_controller-input'), Keys.chord(Keys.ENTER))
WebUI.setText(findTestObject('Page_Create_Assignments/input_date_of_execution'), '23.12.2019')
WebUI.click(findTestObject('Page_Create_Assignments/input__send'))

WebUI.callTestCase(findTestCase('Basic Cases/Logout Case'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Basic Cases/Registrator Login Case (Registrator1.1)'), [:], FailureHandling.STOP_ON_FAILURE)

driver = DriverFactory.getWebDriver()

WebUI.navigateToUrl(filtering_url)

documents = driver.findElements(By.xpath(document_xpath))

if(documents.size() != 0){
	throw new RuntimeException("Документ найден регистратором 1.1")
}

WebUI.callTestCase(findTestCase('Basic Cases/Logout Case'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Basic Cases/Registrator Login Case (Registrator1.2)'), [:], FailureHandling.STOP_ON_FAILURE)

driver = DriverFactory.getWebDriver()

WebUI.navigateToUrl(filtering_url)

documents = driver.findElements(By.xpath(document_xpath))

if(documents.size() != 0){
	throw new RuntimeException("Документ найден регистратором 1.2")
}

WebUI.callTestCase(findTestCase('Basic Cases/Logout Case'), [:], FailureHandling.STOP_ON_FAILURE)

func.signin('ispolnitel_poruchenija_1')

driver = DriverFactory.getWebDriver()

WebUI.navigateToUrl(filtering_url)

document = driver.findElement(By.xpath(document_xpath))

document.click() //detail

if (driver.findElements(By.xpath(change_xpath)).size() != 0){
	throw new RuntimeException("Документ доступен к изменению исполнителем поручения 1")
}

WebUI.callTestCase(findTestCase('Basic Cases/Logout Case'), [:], FailureHandling.STOP_ON_FAILURE)

func.signin('kontroler_poruchenija_1')

driver = DriverFactory.getWebDriver()

WebUI.navigateToUrl(filtering_url)

document = driver.findElement(By.xpath(document_xpath))

document.click() //detail

if (driver.findElements(By.xpath(change_xpath)).size() != 0){
	throw new RuntimeException("Документ доступен к изменению контролером поручения 1")
}

WebUI.callTestCase(findTestCase('Basic Cases/Logout Case'), [:], FailureHandling.STOP_ON_FAILURE)