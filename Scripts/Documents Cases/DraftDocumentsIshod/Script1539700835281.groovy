import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

WebUI.callTestCase(findTestCase('Basic Cases/Registrator Login Case (Veremyanina)'), [:], FailureHandling.STOP_ON_FAILURE)

String url = GlobalVariable.url_to.toString()

WebUI.navigateToUrl(url.concat('documents/outputdocuments/'))

WebUI.delay(3)

WebUI.click(findTestObject('i_add'))

WebUI.click(findTestObject('document test/id_recipient_div'))

WebUI.setText(findTestObject('document test/input_id_recipient'), 'Авиа-Л')

WebUI.click(findTestObject('document test/input_id_recipient_strong_Avia-L'))

WebUI.click(findTestObject('document test/textarea_content_label'))

WebUI.setText(findTestObject('Page_ -/textarea_content'), 'Документ, созданный тестом 7')

WebUI.click(findTestObject('document test/button_from_document_form_register_input'))

WebUI.closeBrowser()

WebUI.callTestCase(findTestCase('Basic Cases/Registrator Login Case (Veremyanina)'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.navigateToUrl(url.concat('documents/outputdocuments/?is_filtering=true&content=Документ%2C+созданный+тестом+7&nomenclature_of_deal=&recipient=&date_of_registration=&registration_number=&signed=&implementer=&status='))

WebUI.delay(3)

WebUI.click(findTestObject('click_task'))

WebUI.delay(3)

WebUI.click(findTestObject('click_change'))

WebUI.click(findTestObject('i_add'))

WebUI.delay(3)

WebUI.uploadFile(findTestObject('document test/input_with_name_attachment_set-0-attachment'), GlobalVariable.filename)

WebUI.delay(3)

WebUI.click(findTestObject('document test/button_from_document_form_save_input'))

