import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import func

String assignmentDescription = func.randomString(10)

WebUI.callTestCase(findTestCase('Basic Cases/Registrator Login Case (Melnikova)'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_With_Menu/a_MyAssignments'))

WebUI.delay(3)

WebUI.click(findTestObject('Page_Assignments_List_View/add_new_assignment_button'))

WebUI.delay(3)

WebUI.setText(findTestObject('Page_Create_Assignments/textarea_description'), assignmentDescription)

WebUI.click(findTestObject('Page_Create_Assignments/add_new_implementer_button'))

WebUI.delay(3)

WebUI.setText(findTestObject('Page_Create_Assignments/input_id_coimplementer_set-0-c'), 'Мел')

WebUI.delay(3)

WebUI.click(findTestObject('Page_Create_Assignments/strong_Mel'))

WebUI.setText(findTestObject('Page_Create_Assignments/input_id_coimplementer_set-1-c'), 'Корч')

WebUI.delay(3)

WebUI.click(findTestObject('Page_Create_Assignments/strong_Korch'))

WebUI.click(findTestObject('Page_Create_Assignments/add_new_attachment_button'))

WebUI.delay(3)

WebUI.uploadFile(findTestObject('Page_Create_Assignments/input_assignment_set-0-attachm'), GlobalVariable.filename)

WebUI.click(findTestObject('Page_Create_Assignments/input__save'))

String url = GlobalVariable.url_to.toString()

WebUI.navigateToUrl(url.concat('assignments/draft_assignments/?is_filtering=true&date_of_execution=&coimplementers=&author=62&controller=&connected_object=&description=' + assignmentDescription))

WebUI.click(findTestObject('click_task'))

WebUI.delay(3)

WebUI.click(findTestObject('click_change'))

WebUI.delay(3)

WebUI.click(findTestObject('click_sent'))

WebUI.closeBrowser()

WebUI.callTestCase(findTestCase('Basic Cases/Registrator Login Case (Melnikova)'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.navigateToUrl(url.concat('assignments2me/?is_filtering=true&date_of_execution=&author=62&controller=&current_status__status=execution&description='+ assignmentDescription))

WebUI.click(findTestObject('click_task'))

WebUI.delay(3)

WebUI.click(findTestObject('click_change'))

WebUI.delay(3)

WebUI.click(findTestObject('click_answer'))

WebUI.setText(findTestObject('textarea_answer'), 'ответ')

WebUI.click(findTestObject('Page_Create_Assignments/add_new_attachment_button_with_answer'))

WebUI.delay(3)

WebUI.uploadFile(findTestObject('document test/history_set-0-attachm'), GlobalVariable.filename)

WebUI.click(findTestObject('input__execute'))

WebUI.delay(3)

WebUI.closeBrowser()

WebUI.callTestCase(findTestCase('Basic Cases/Registrator Login Case (Korchagin)'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.navigateToUrl(url.concat('assignments2me/?is_filtering=true&date_of_execution=&author=62&controller=&current_status__status=execution&description=' + assignmentDescription))

WebUI.click(findTestObject('click_task'))

WebUI.delay(3)

WebUI.click(findTestObject('click_change'))

WebUI.delay(3)

WebUI.click(findTestObject('click_answer'))

WebUI.delay(3)

WebUI.setText(findTestObject('textarea_answer'), 'ответ')

WebUI.click(findTestObject('Page_Create_Assignments/add_new_attachment_button_with_answer'))

WebUI.delay(3)

WebUI.uploadFile(findTestObject('document test/history_set-0-attachm'), GlobalVariable.filename)

WebUI.click(findTestObject('input__execute'))

WebUI.callTestCase(findTestCase('Basic Cases/Logout Case'), [:], FailureHandling.STOP_ON_FAILURE)

