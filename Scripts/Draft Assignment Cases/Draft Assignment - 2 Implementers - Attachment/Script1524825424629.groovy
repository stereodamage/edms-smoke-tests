import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Basic Cases/Registrator Login Case (Melnikova)'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Page_With_Menu/a_MyAssignments'))

WebUI.click(findTestObject('Page_Assignments_List_View/add_new_assignment_button'))

WebUI.setText(findTestObject('Page_Create_Assignments/textarea_description'), 'Поручение автотестов 2исп')

//WebUI.click(findTestObject('Page_Create_Assignments/add_new_implementer_button'))
WebUI.click(findTestObject('Page_Create_Assignments/add_new_implementer_button'))

WebUI.setText(findTestObject('Page_Create_Assignments/input_id_coimplementer_set-0-c'), 'Вере')

WebUI.click(findTestObject('Page_Create_Assignments/strong_Vere'))

WebUI.setText(findTestObject('Page_Create_Assignments/input_id_coimplementer_set-1-c'), 'Аза')

WebUI.click(findTestObject('Page_Create_Assignments/strong_Aza'))

WebUI.click(findTestObject('Page_Create_Assignments/add_new_attachment_button'))

WebUI.delay(3)

WebUI.uploadFile(findTestObject('Page_Create_Assignments/input_assignment_set-0-attachm'), GlobalVariable.filename)

WebUI.click(findTestObject('Page_Create_Assignments/input__save'))

WebUI.callTestCase(findTestCase('Basic Cases/Logout Case'), [:], FailureHandling.STOP_ON_FAILURE)

