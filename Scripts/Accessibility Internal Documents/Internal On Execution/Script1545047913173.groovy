import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
// import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import func

String url = GlobalVariable.url_to.toString()

String documentContent = func.randomString(10)

func.signin('avtor_1')

driver = DriverFactory.getWebDriver()

WebUI.click(findTestObject('document test/add_document_internal'))

WebUI.click(findTestObject('document test/i_add_document'))

WebUI.setText(findTestObject('document test/input_id_addressee-input'), 'Полу')

WebUI.click(findTestObject('document test/Page_doc_create/div_addressee'))

WebUI.setText(findTestObject('document test/input_id_approved-internal'), 'Отве')

WebUI.click(findTestObject('document test/Page_doc_create/div_responsible'))

WebUI.setText(findTestObject('document test/input_id_implementer'), 'Реги')

WebUI.click(findTestObject('document test/Page_doc_create/div_implementer'))

WebUI.setText(findTestObject('document test/textarea_content'), documentContent)

WebUI.click(findTestObject('document test/button_from_document_form_save_input'))

WebUI.callTestCase(findTestCase('Basic Cases/Logout Case'), [:], FailureHandling.STOP_ON_FAILURE)

func.signin('registrator_1')

driver = DriverFactory.getWebDriver()

WebUI.navigateToUrl(url + 'documents/internaldocuments/?is_filtering=true&content=' + documentContent)

WebUI.click(findTestObject('document test/document'))

connect = driver.findElement(By.xpath("//a[contains(text(),'Изменить')]"))

connect.click()

WebUI.click(findTestObject('document test/button_from_document_form_on_signing_input')) // on signing

WebUI.callTestCase(findTestCase('Basic Cases/Logout Case'), [:], FailureHandling.STOP_ON_FAILURE)

func.signin('otvetstvennyj_1')

driver = DriverFactory.getWebDriver()

WebUI.navigateToUrl(url + 'documents/internaldocuments/?is_filtering=true&content=' + documentContent)

WebUI.click(findTestObject('document test/document'))

connect = driver.findElement(By.xpath("//button[contains(text(),'Подписать')]")) //sign

connect.click()

WebUI.navigateToUrl(url + 'documents/internaldocuments/?is_filtering=true&content=' + documentContent)

WebUI.click(findTestObject('document test/document'))

WebUI.click(findTestObject('document test/click_create_assignment')) //on execution

String assignmentDescription = "Поручение по ".concat(documentContent)

WebUI.setText(findTestObject('Page_Create_Assignments/textarea_description'), assignmentDescription)
WebUI.setText(findTestObject('Page_Create_Assignments/input_id_coimplementer_set-0-c'), 'Испо')
WebUI.sendKeys(findTestObject('Page_Create_Assignments/input_id_coimplementer_set-0-c'), Keys.chord(Keys.DOWN))
WebUI.sendKeys(findTestObject('Page_Create_Assignments/input_id_coimplementer_set-0-c'), Keys.chord(Keys.ENTER))
WebUI.click(findTestObject('Page_Create_Assignments/label_Controller'))
WebUI.setText(findTestObject('Page_Create_Assignments/input_id_controller-input'), 'Контр')
WebUI.sendKeys(findTestObject('Page_Create_Assignments/input_id_controller-input'), Keys.chord(Keys.DOWN))
WebUI.sendKeys(findTestObject('Page_Create_Assignments/input_id_controller-input'), Keys.chord(Keys.ENTER))
WebUI.setText(findTestObject('Page_Create_Assignments/input_date_of_execution'), '23.12.2019')
WebUI.click(findTestObject('Page_Create_Assignments/input__send'))

WebUI.callTestCase(findTestCase('Basic Cases/Logout Case'), [:], FailureHandling.STOP_ON_FAILURE)

func.signin('avtor_1')

driver = DriverFactory.getWebDriver()

WebUI.navigateToUrl(url + 'documents/internaldocuments/?is_filtering=true&content=' + documentContent)

WebUI.click(findTestObject('document test/document'))

if (driver.findElements(By.xpath("//a[(text() = 'Изменить' or . = 'Изменить')]")).size() != 0){
	throw new RuntimeException("Документ доступен к изменению автором 1")
}

WebUI.callTestCase(findTestCase('Basic Cases/Logout Case'), [:], FailureHandling.STOP_ON_FAILURE)

func.signin('otvetstvennyj_1')

driver = DriverFactory.getWebDriver()

WebUI.navigateToUrl(url + 'documents/internaldocuments/?is_filtering=true&content=' + documentContent)

WebUI.click(findTestObject('document test/document'))

if (driver.findElements(By.xpath("//a[(text() = 'Изменить' or . = 'Изменить')]")).size() != 0){
	throw new RuntimeException("Документ доступен к изменению ответсвенным 1")
}

WebUI.callTestCase(findTestCase('Basic Cases/Logout Case'), [:], FailureHandling.STOP_ON_FAILURE)

func.signin('poluchatel_1')

driver = DriverFactory.getWebDriver()

WebUI.navigateToUrl(url + 'documents/internaldocuments/?is_filtering=true&content=' + documentContent)

WebUI.click(findTestObject('document test/document'))

if (driver.findElements(By.xpath("//a[(text() = 'Изменить' or . = 'Изменить')]")).size() != 0){
	throw new RuntimeException("Документ доступен к изменению получателем 1")
}

WebUI.callTestCase(findTestCase('Basic Cases/Logout Case'), [:], FailureHandling.STOP_ON_FAILURE)

func.signin('registrator_1')

driver = DriverFactory.getWebDriver()

WebUI.navigateToUrl(url + 'documents/internaldocuments/?is_filtering=true&content=' + documentContent)

WebUI.click(findTestObject('document test/document'))

if (driver.findElements(By.xpath("//a[(text() = 'Изменить' or . = 'Изменить')]")).size() != 0){
	throw new RuntimeException("Документ доступен к изменению регистратором 1")
}

WebUI.callTestCase(findTestCase('Basic Cases/Logout Case'), [:], FailureHandling.STOP_ON_FAILURE)

func.signin('ispolnitel_poruchenija_1')

driver = DriverFactory.getWebDriver()

WebUI.navigateToUrl(url + 'documents/internaldocuments/?is_filtering=true&content=' + documentContent)

WebUI.click(findTestObject('document test/document'))

if (driver.findElements(By.xpath("//a[(text() = 'Изменить' or . = 'Изменить')]")).size() != 0){
	throw new RuntimeException("Документ доступен к изменению исполнителем поручения 1")
}

WebUI.callTestCase(findTestCase('Basic Cases/Logout Case'), [:], FailureHandling.STOP_ON_FAILURE)

func.signin('kontroler_poruchenija_1')

driver = DriverFactory.getWebDriver()

WebUI.navigateToUrl(url + 'documents/internaldocuments/?is_filtering=true&content=' + documentContent)

WebUI.click(findTestObject('document test/document'))

if (driver.findElements(By.xpath("//a[(text() = 'Изменить' or . = 'Изменить')]")).size() != 0){
	throw new RuntimeException("Документ доступен к изменению контролером поручения 1")
}

WebUI.callTestCase(findTestCase('Basic Cases/Logout Case'), [:], FailureHandling.STOP_ON_FAILURE)