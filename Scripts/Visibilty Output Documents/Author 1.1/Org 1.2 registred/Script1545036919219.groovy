import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.Keys as Keys
import func as func

String documentContent = func.randomString(10)

String url = GlobalVariable.url_to.toString()

func.signin('avtor_2')

driver = DriverFactory.getWebDriver()

WebUI.click(findTestObject('document test/output_document'))

WebUI.delay(2)

WebUI.click(findTestObject('document test/add_output_document'))

WebUI.delay(2)

WebUI.setText(findTestObject('document test/input_id_recipient'), 'а')

WebUI.sendKeys(findTestObject('document test/input_id_recipient'), Keys.chord(Keys.DOWN))
WebUI.sendKeys(findTestObject('document test/input_id_recipient'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('document test/id_signed-input'), 'Ответ')

WebUI.sendKeys(findTestObject('document test/id_signed-input'), Keys.chord(Keys.DOWN))
WebUI.sendKeys(findTestObject('document test/id_signed-input'), Keys.chord(Keys.DOWN))
WebUI.sendKeys(findTestObject('document test/id_signed-input'), Keys.chord(Keys.DOWN))
WebUI.sendKeys(findTestObject('document test/id_signed-input'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('document test/input_id_implementer'), 'Испол')

WebUI.sendKeys(findTestObject('document test/input_id_implementer'), Keys.chord(Keys.DOWN))
WebUI.sendKeys(findTestObject('document test/input_id_implementer'), Keys.chord(Keys.DOWN))
WebUI.sendKeys(findTestObject('document test/input_id_implementer'), Keys.chord(Keys.DOWN))
WebUI.sendKeys(findTestObject('document test/input_id_implementer'), Keys.chord(Keys.ENTER))

WebUI.sendKeys(findTestObject('document test/id_organization'), Keys.chord(Keys.DOWN))
WebUI.sendKeys(findTestObject('document test/id_organization'), Keys.chord(Keys.DOWN))
WebUI.sendKeys(findTestObject('document test/id_organization'), Keys.chord(Keys.ENTER))


WebUI.delay(2)

WebUI.setText(findTestObject('document test/textarea_content'), documentContent)

WebUI.click(findTestObject('document test/button_from_document_form_save_input'))

WebUI.closeBrowser()

WebUI.callTestCase(findTestCase('Basic Cases/Registrator Login Case (Registrator1.2)'), [:], FailureHandling.STOP_ON_FAILURE)

driver = DriverFactory.getWebDriver()

WebUI.navigateToUrl(url.concat('documents/outputdocuments/?is_filtering=true&content=' + documentContent))

document = driver.findElement(By.xpath(('//*[@id="table"]/tbody/tr/td//a[text()="' + documentContent) + '"]'))

document.click() //detail

WebUI.click(findTestObject('click_change')) //update

WebUI.click(findTestObject('document test/button_from_document_form_register_input')) //register

WebUI.closeBrowser()

func.signin('avtor_2')

driver = DriverFactory.getWebDriver()

WebUI.navigateToUrl(((url + 'documents/outputdocuments/?is_filtering=true&content=') + documentContent) + '&nomenclature_of_deal=&sender=&date_of_registration=&registration_number=&output_date=&output_number=&cipher=')

WebUI.delay(2)

WebUI.click(findTestObject('document test/document')) // detail

if(driver.findElements(By.xpath("//a[(text() = 'Изменить' or . = 'Изменить')]")).size() != 0){
	throw new RuntimeException("Документ доступен к изменению автором 1.1")
}

WebUI.delay(2)

WebUI.closeBrowser()

WebUI.callTestCase(findTestCase('Basic Cases/Registrator Login Case (Registrator1)'), [:], FailureHandling.STOP_ON_FAILURE)

driver = DriverFactory.getWebDriver()

WebUI.navigateToUrl(url.concat('documents/outputdocuments/?is_filtering=true&content=' + documentContent))

document = driver.findElements(By.xpath(('//*[@id="table"]/tbody/tr/td//a[text()="' + documentContent) + '"]'))

if(document.size()){
	throw new RuntimeException("Документ найден регистратором 1")
}

WebUI.closeBrowser()

WebUI.callTestCase(findTestCase('Basic Cases/Registrator Login Case (Registrator1.1)'), [:], FailureHandling.STOP_ON_FAILURE)

driver = DriverFactory.getWebDriver()

WebUI.navigateToUrl(url.concat('documents/outputdocuments/?is_filtering=true&content=' + documentContent))

document = driver.findElements(By.xpath(('//*[@id="table"]/tbody/tr/td//a[text()="' + documentContent) + '"]'))

if(document.size()){
	throw new RuntimeException("Документ найден регистратором 1.1")
}

WebUI.closeBrowser()

func.signin('avtor_1')

driver = DriverFactory.getWebDriver()

WebUI.navigateToUrl(url.concat('documents/outputdocuments/?is_filtering=true&content=' + documentContent))

document = driver.findElements(By.xpath(('//*[@id="table"]/tbody/tr/td//a[text()="' + documentContent) + '"]'))

if(document.size()){
	throw new RuntimeException("Документ найден автором 1")
}

WebUI.closeBrowser()

func.signin('avtor_3')

driver = DriverFactory.getWebDriver()

WebUI.navigateToUrl(url.concat('documents/outputdocuments/?is_filtering=true&content=' + documentContent))

document = driver.findElements(By.xpath(('//*[@id="table"]/tbody/tr/td//a[text()="' + documentContent) + '"]'))

if(document.size()){
	throw new RuntimeException("Документ найден автором 1.2")
}

WebUI.closeBrowser()

func.signin('otvetstvennyj_3')

driver = DriverFactory.getWebDriver()

WebUI.navigateToUrl(url.concat('documents/outputdocuments/?is_filtering=true&content=' + documentContent))

document = driver.findElements(By.xpath(('//*[@id="table"]/tbody/tr/td//a[text()="' + documentContent) + '"]'))

WebUI.click(findTestObject('document test/document')) // detail

if(driver.findElements(By.xpath("//a[(text() = 'Изменить' or . = 'Изменить')]")).size() != 0){
	throw new RuntimeException("Документ доступен к изменению ответственным 1.2")
}

WebUI.closeBrowser()

func.signin('ispolnitel_poruchenija_3')

driver = DriverFactory.getWebDriver()

WebUI.navigateToUrl(url.concat('documents/outputdocuments/?is_filtering=true&content=' + documentContent))

document = driver.findElements(By.xpath(('//*[@id="table"]/tbody/tr/td//a[text()="' + documentContent) + '"]'))

WebUI.click(findTestObject('document test/document')) // detail

if(driver.findElements(By.xpath("//a[(text() = 'Изменить' or . = 'Изменить')]")).size() != 0){
	throw new RuntimeException("Документ доступен к изменению исполнителем 1.2")
}

WebUI.closeBrowser()