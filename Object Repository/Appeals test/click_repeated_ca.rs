<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>click_repeated_ca</name>
   <tag></tag>
   <elementGuidId>91925ff7-6ab1-4b5f-bd66-00527283c419</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[@href = '/assignments2me/1202/change/' and (text() = 'Изменить' or . = 'Изменить')]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[(text() = Добавить повторное' or . = 'Добавить повторное')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>waves-effect waves-light btn white-text</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/assignments2me/1202/change/</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Изменить</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;with-sidebar&quot;]/main[1]/div[@class=&quot;content&quot;]/div[@class=&quot;left-panel wide&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-content&quot;]/a[@class=&quot;waves-effect waves-light btn white-text&quot;]</value>
   </webElementProperties>
</WebElementEntity>
