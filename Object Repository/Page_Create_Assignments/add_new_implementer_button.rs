<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>add_new_implementer_button</name>
   <tag></tag>
   <elementGuidId>69e0ec38-e34d-41cf-80f0-f9189f6ab0d4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//i[(text() = 'add' or . = 'add')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>i</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>large material-icons</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>add</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;with-sidebar&quot;]/main[1]/div[@class=&quot;content&quot;]/div[@class=&quot;fixed-action-btn active&quot;]/a[@class=&quot;btn-floating btn-large waves-effect waves-light red z-depth-2&quot;]/i[@class=&quot;large material-icons&quot;]</value>
   </webElementProperties>
</WebElementEntity>
