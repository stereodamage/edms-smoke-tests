<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Assignment Testing</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-06-21T13:56:38</lastRun>
   <mailRecipient>ivashin.alexei@gmail.com;</mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>e01faee7-7924-42f4-b139-05aa70efb8b3</testSuiteGuid>
   <testCaseLink>
      <guid>8b4eac8a-6fda-43e7-9941-288225e45950</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Assignments Cases/Assignment - 2 Implementers - Attachment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>32d8389d-ac71-4474-8393-539c6f4af742</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Assignments Cases/Assignment - 2 Implementers - Controller - Attachment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c31a24dd-8e8a-47ba-93d5-df10cea27887</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Assignments Cases/Assignment - 2 Implementers - Controller</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6e936af1-7f28-41fd-acec-c6c41c8e7c46</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Assignments Cases/Assignment - 2 Implementers - Equal implementers</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>44c69162-2a15-4c87-9b39-23ff9b626c8f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Assignments Cases/Assignment - 2 Implementers</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b413e150-96e7-4f36-b352-368ce347dc8b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Assignments Cases/Assignment - 3 Implementer - Attachment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>02c92f0c-59ee-461c-9b86-38b95def9301</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Assignments Cases/Assignment - 3 Implementer - Controller - Attachment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>adf32c8d-16b5-4355-9f6d-ddd76ddcff95</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Assignments Cases/Assignment - 3 Implementer - Controller</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b54a3484-ec4e-4005-8e51-935963ff269b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Assignments Cases/Assignment - 3 Implementer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c812a0f6-a4e6-49e2-a0ae-2a00353e648f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Assignments Cases/Assignment - Attachment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9e14c963-bda8-4237-880f-80f9fac561b4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Assignments Cases/Assignment - Controller - Attachment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2409aa84-beb9-4bdc-b28e-17f4f6b44e19</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Assignments Cases/Assignment - Controller</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d203d08b-1dbc-4306-9d5e-08f4b105d394</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Assignments Cases/Assignment - Filters - Change status to CANCELED</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ed60a39d-2586-47fb-8e63-66800416d519</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Assignments Cases/Assignment - Filters - Change status to EXECUTED</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>202e1431-d89b-4094-b0ef-bf3fa09055b8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Assignments Cases/Assignment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a6955404-2548-4242-bedd-4a44b4d25481</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Basic Cases/Create Input Document Case</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0069a976-399c-47b7-a2bd-02e88f841255</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Assignment With Form Errors/Assignment - Controller with no date - Send</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f4eb14d1-59ac-4f95-890a-97b47fd2f9bc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Assignment With Form Errors/Assignment - Empty Description - Send</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>caf4647b-a67b-4bf4-ad12-0faab367937a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Assignment With Form Errors/Assignment - Empty implementer list - Send</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ddeb2b20-4081-49b3-a0a3-faa388f550d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Assignment With Form Errors/Draft - Attachment - ErrorsTest - Save</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>80b8db41-074f-439f-b05c-b842d8c333ab</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Assignment With Form Errors/Draft Assignment - Controller with no date</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0b1a39e6-fe94-4795-9cf5-99e84a1bee69</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Assignment With Form Errors/Draft Assignment - Empty Description</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6fe2bf1f-3677-4362-92d5-7d27ebd55ef7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Assignment With Form Errors/Draft Assignment - Empty implementer list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d632fe12-ef51-4c22-8487-7174a7dfa16d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Input Documents/Create document with attachments - Register - Add attachments</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3a8baf1b-8880-4057-9d9b-64206f5f215d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Input Documents/Create document - Change addressee - Register - Add attachments</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9a8b184a-687e-4f07-9f8d-40d80bccc501</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Input Documents/Create document - Change addressee and responsible - Register - Add attachments</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0f8cbe0b-b3e8-45d9-8690-a6ecd79df698</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Input Documents/Draft - Register - Add attachments</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9a23d78d-f4a4-43e5-b9f0-b58e74686598</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Documents Cases/DraftDocumentsIshod</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d3d3bad3-5d2c-453c-a664-9349e7fe968b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Documents Cases/DraftDocumentsOtvetstvenniy</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e192f779-5546-47c8-8ae8-15610ef7ba9e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Output Documents/Create document - Change signed - Register - Add attachments</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b033c31f-3ad3-4f8a-9ee0-3ace1c2b0868</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Output Documents/Create document - Change signed and implementer - Register - Add attachments</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ad1b12e2-25a6-41fd-8719-d0b5cf22e09c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Documents Cases/DraftDocumentVloshenia-2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c2a7f634-b3bb-4ddd-b23e-2de034a722a3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Draft Assignment Cases/1 Draft Assignment - 2 Implementers - Attachment - Search</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e8b4d15c-b762-4f4b-96c4-0177f48136a5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Draft Assignment Cases/1.1Draft inbound document</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9d6c54b7-1ced-4187-ad96-2f879ac7f319</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Draft Assignment Cases/1.2Draft inbound document</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8763e5a6-0d1a-4ec7-8385-1f73ba56c690</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Draft Assignment Cases/2 Draft Assignment - 2 Implementers - Attachment - Search</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e6d7eb4c-8732-4187-8b6d-1edbabbebc69</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Draft Assignment Cases/Draft - Attachment - Controller - Save</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2ffb5348-f280-4065-97cc-4c9470eddba5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Draft Assignment Cases/Draft - Attachment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>479be677-6a2d-49e1-8d41-bdd774c7e599</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Draft Assignment Cases/Draft - Controller</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6d24f6f8-4454-47cd-ba15-517985343364</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Draft Assignment Cases/Draft Assignment - 2 Implementers - Attachment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c16c82eb-9b50-4de8-83fa-be8fca749bef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Draft Assignment Cases/Draft Assignment - 2 Implementers - Responsible - Attachment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6935647b-8c0c-46fc-aa5a-d985d4a2e14a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Draft Assignment Cases/Draft Assignment - 2 Implementers - Responsible</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9e9c9d6a-9cdc-415e-aed9-932881c9a4a7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Draft Assignment Cases/Draft Assignment - 2 Implementers</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>62c1d9a6-52a2-4ed8-ae89-a2254bc59bd0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Draft Assignment Cases/Draft Assignment - 3 Implementer - Attachment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fbe28f51-acbb-48ca-bbfc-06214ba20686</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Draft Assignment Cases/Draft Assignment - 3 Implementer - Responsible - Attachment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fa6a9cd4-4375-4d5a-b4d0-ae81192baaf5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Draft Assignment Cases/Draft Assignment - 3 Implementer - Responsible</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>94bd5889-b188-4220-b289-3ba4bd73a607</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Draft Assignment Cases/Draft Assignment - 3 Implementer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>78d455d6-97fe-44e0-a606-43dff568fe52</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Draft Assignment Cases/Draft Assignment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5ad1d3dc-cba4-4a8b-ab2e-d5dcd15106e0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Draft Executive Documents/Draft Executive Documents Save all Field</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>396d34fe-b4b1-45f1-a577-cc84ff788f32</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Draft Executive Documents/Draft Executive Documents Save</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>111c2ffe-6b43-4e2b-9172-ac8505b3f915</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Draft Executive Documents/Draft Executive Documents</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f47eb967-07f2-42b4-87b7-1edc7c015c96</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Draft Input Documents/Draft Input Documents Save all Field</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eacd8437-9e16-4258-ae86-9b0a6c7a7802</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Draft Input Documents/Draft Input Documents Save</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>42e424de-8d14-4a6f-bb5e-1f31a287a4c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Draft Input Documents/Draft Input Documents</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>85ff6aca-8985-4ae9-89da-68a578b87acf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Draft Interal Documents/Draft Internal Documents Save all Field</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ec14b455-21d6-4a69-9d50-e8d4e9023771</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Draft Interal Documents/Draft Internal Documents Save</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>943abf91-ec73-404f-86d8-6c79af0acc5e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Draft Interal Documents/Draft Internal Documents</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>df0d9c54-b82d-4a51-af5e-57b1574a047b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Draft Output Documents/Draft Output Documents Save all Field</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a49c7a63-ca95-4cde-8dfa-ab33c8907447</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Draft Output Documents/Draft Output Documents Save</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2d3b37e6-7734-4b05-b8f4-70b30578fcf1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Draft Output Documents/Draft Output Documents</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c51ee36f-1664-4d6a-b17b-6cb1bf86853b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Output Documents/Create document - Change implementer - Register - Add attachments</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e344f6f1-c9f6-4597-ade6-b14b12fd89ae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Output Documents/Draft - Register - Add attachments</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b8845eac-c6e5-4512-9f5e-6991d4f36998</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Visibility Input Documents/Reg1 org1 not registred</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1b77ec6a-47e7-40fd-9093-02f3abceff32</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Visibility Input Documents/Reg1 org1 registred</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b67b4ed2-1d6d-49be-89c7-d68b42de6ed1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Visibility Input Documents/Reg1.1 org1 not registred</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7ae99b37-eb29-4b58-aa54-ca7bbae90aab</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Visibility Input Documents/Reg1.1 org1 registred</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6c3defd7-a799-4a30-b130-3b47b50b68b9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Visibility Input Documents/Reg1.1 org1.1 registred</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3d1dba9d-32f0-4ce2-97f0-25d6711727ed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Visibility Input Documents/Reg1.1 org1.1. not registred</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e65dd736-93ef-4fd2-b420-d8787b57f789</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Visibility Input Documents/Reg1.1 org1.2 not registred</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e83d14c2-01a0-4dda-b991-a59a9306d594</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Visibility Input Documents/Reg1.1 org1.2 registred</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d38498cb-a6b6-4686-a143-14b823305dd2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Accessibility Input Documents/Connection To Unregistered Document Case</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>be5319ac-01b5-41db-8512-6e058bfbac22</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Visibilty Output Documents/Author 1/Org 1 not registred</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1d8c9c0b-b5f3-41dc-ae22-652a9c043b0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Visibilty Output Documents/Author 1/Org 1 registred</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>45371849-e6b2-4342-bd28-8549fbb2d685</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Visibilty Output Documents/Author 1/Org 1.1 not registred</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>44790bf2-abff-4f83-b59c-215f3f985c0f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Visibilty Output Documents/Author 1/Org 1.1 registred</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>09d29ff7-6c3c-4641-b8ba-52b91074ebfe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Visibilty Output Documents/Author 1.1/Org 1 not registred</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>31d513f7-9b6b-440a-8905-f0e53de7c873</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Visibilty Output Documents/Author 1.1/Org 1 registred</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>72bd302c-c539-46c1-9b44-fe6d8d662d38</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Visibilty Output Documents/Author 1.1/Org 1.1 not registred</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9abc7b1c-92a8-4759-a45b-7222f2c8fec1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Visibilty Output Documents/Author 1.1/Org 1.1 registred</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3ae7181f-463e-4003-908c-3ac7aaf5e0b3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Visibilty Output Documents/Author 1.1/Org 1.2 not registred</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c557206c-9814-4f37-8cda-877864e70c50</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Visibilty Output Documents/Author 1.1/Org 1.2 registred</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bf424851-44ba-407f-acdb-6be686ad1478</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Accessibility Input Documents/Connection to Registered Document Case</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>52f4618b-2ba3-4df9-a1dc-5955dc2559af</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Accessibility Input Documents/Connection To Unregistered Document Case</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a4fe2359-b68f-486a-83aa-1688fcf6599f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Accessibility Internal Documents/Internal Under Consideration</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4f1f467c-1999-441d-a0f1-bc39005423f5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Accessibility Internal Documents/Internal On Signing</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4b8c70ec-b5ea-47f1-8823-8d8dccedcfb0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Accessibility Internal Documents/Internal On Execution</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a5cd3f18-cbaa-4742-870a-a167ec0f9c86</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Accessibility Internal Documents/Internal Draft</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>714d830a-e18b-44c3-b57d-6c63c5b86660</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CitizenAppeal/Dossier create</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1a6744c9-c15f-4401-8dca-f94f14d673a6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CitizenAppeal/Dossier create - CA create from dossier</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dd4d2174-c247-46d8-bdad-0c16208dd821</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CitizenAppeal/Dossier create - CA create from dossier - Create repeated</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a0e18d51-ba90-4d98-a60c-841725b3bd7e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CitizenAppeal/CA create - dossier choice</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
